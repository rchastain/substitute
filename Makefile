
all: substitute

NEG: NEG.c
	gcc -o $@ $<

substitute: substitute.pas
	fpc @build.cfg $<

demo: substitute NEG
	xboard -debug -fcp "./substitute NEG" 
