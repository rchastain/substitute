
if [ -z "$1" ]
then
  BUILDMODE=DEBUG
else
  BUILDMODE=$1
fi

fpc -B substitute.pas @build.cfg -d$BUILDMODE
