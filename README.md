# Substitute

## Overview

*Substitute* (1) is an application that relays the messages exchanged between a user and another application.

In my case, the other application is a chess engine, but it can be any application that has a dialog with the user using standard input and output.

(1) Maybe *spy* would have been a better name.

## Usage

*Substitute* is a command line tool of the same kind (although much less sophisticated) as [InBetween](https://www.chessprogramming.org/InBetween) by Odd Gunnar Malin, or as [UciFilter](http://www.nnuss.de/Hermann/UciFilter.html) by Volker Annuss.

Instead of starting an engine, you start *Substitute* with the engine name specified, either as parameter, or in configuration file (**substitute.ini**). *Substitute* creates a file (**substitute.log**) where you can see the messages sent by the user and the one sent by the engine.

## Build

*Substitute* is a Pascal program, for the Free Pascal Compiler. It has been tested only under Linux, but should work also under other environments.
